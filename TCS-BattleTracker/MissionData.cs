﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker {
  public class MissionData {
    public string WorldName { get; set; }
    public Vector3 WorldSize { get; set; }
    public string MissionName { get; set; }
    public float Timestamp { get; set; }
  }
}
