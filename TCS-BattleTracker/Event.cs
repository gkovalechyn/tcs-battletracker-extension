﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker {
  public abstract class Event {
    private readonly float timestamp;

    public Event(float timestamp) {
      this.timestamp = timestamp;
    }

    public float getTimestamp() {
      return this.timestamp;
    }

    public abstract string getType();
  }
}
