﻿using System;

namespace TCS_BattleTracker {
  /// <summary>
  /// Represents a arma 3 position where:
  ///   X points to the right on the map screen.
  ///   Y points up on the map screen.
  ///   Z points "up" on the map screen. (In the direction of the viewer)
  ///   
  /// In a 3d context:
  ///   X points "Right"
  ///   Y points "Forward"
  ///   Z points "Up"
  /// </summary>
  public struct Vector3 {
    public float x;
    public float y;
    public float z;

    public Vector3(float x, float y, float z) {
      this.x = x;
      this.y = y;
      this.z = z;
    }

    public static Vector3 operator +(Vector3 lhs, Vector3 rhs) {
      return new Vector3(
          lhs.x + rhs.x,
          lhs.y + rhs.y,
          lhs.z + rhs.z
      );
    }

    public static Vector3 operator -(Vector3 lhs, Vector3 rhs) {
      return new Vector3(
          lhs.x - rhs.x,
          lhs.y - rhs.y,
          lhs.z - rhs.z
      );
    }

    public double magnitude() {
      return Math.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public double squaredMagnitude() {
      return this.x * this.x + this.y * this.y + this.z * this.z;
    }



    /// <summary>
    /// Returns a Vector 3 from an arma 3 position array, being it 2 dimensional or 3 dimensional.
    /// </summary>
    /// <param name="a3Array">An arma 3 position array, being it in the format [x, y, z] or [x, y].</param>
    /// <returns>The vector3 representing that position</returns>
    public static Vector3 fromArma3Array(string a3Array) {
      a3Array = a3Array.Trim();
      //Remove both "[" and "]"
      a3Array = a3Array.Substring(1, a3Array.Length - 2);
      string[] parts = a3Array.Split(',');

      if (parts.Length != 2 && parts.Length != 3) {
        Logger.error("Vector3::fromArma3Array - Wrong position format, expected 2 or 3 components but got " + parts.Length);
        return new Vector3(0, 0, 0);
      } else {
        Vector3 result = new Vector3(
          float.Parse(parts[0]),
          float.Parse(parts[1]),
          0
        );

        if (parts.Length == 3) {
          result.z = float.Parse(parts[2]);
        }

        return result;
      }
    }
  }
}