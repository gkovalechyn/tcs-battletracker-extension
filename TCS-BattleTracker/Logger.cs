﻿using System;
using System.Globalization;
using System.IO;

namespace TCS_BattleTracker {
  class Logger {
    private static Logger instance;
    private StreamWriter writer;
    private LogLevel currentLevel = LogLevel.DEBUG;

    public Logger(string file) {
      writer = new StreamWriter(file);
    }

    public static void debug(string message) {
      Logger.log(LogLevel.DEBUG, message);
    }

    public static void info(string message) {
      Logger.log(LogLevel.INFO, message);
    }

    public static void warning(string message) {
      Logger.log(LogLevel.WARNING, message);
    }

    public static void error(string message) {
      Logger.log(LogLevel.ERROR, message);
    }

    public static void log(LogLevel level, string message) {
      Logger.checkInstance();

      if (level >= Logger.instance.currentLevel) {
        string timeString = DateTime.Now.ToString(CultureInfo.InvariantCulture.DateTimeFormat);

        Logger.instance.writer.Write(timeString);
        Logger.instance.writer.Write("[" + Logger.logLevelToString(level) + "]");
        Logger.instance.writer.WriteLine(message);
        Logger.instance.writer.Flush();
      }
    }

    private static void checkInstance() {
      if (Logger.instance == null) {
        Logger.instance = new Logger("BattleTracker.log");
      }
    }

    public static string logLevelToString(LogLevel level) {
      switch (level) {
        case LogLevel.DEBUG:
          return "DEBUG";
        case LogLevel.INFO:
          return "INFO";
        case LogLevel.WARNING:
          return "WARNING";
        case LogLevel.ERROR:
          return "ERROR";
        default:
          throw new InvalidDataException("Unkown log level" + level);
      }
    }
  }
}
