﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Converters {
  class VehicleConverter : JsonConverter<Vehicle> {
    public override bool CanRead => false;

    public override bool CanWrite => base.CanWrite;

    public override Vehicle ReadJson(JsonReader reader, Type objectType, Vehicle existingValue, bool hasExistingValue, JsonSerializer serializer) {
      throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, Vehicle value, JsonSerializer serializer) {
      writer.WriteStartObject();

      writer.WritePropertyName("id");
      writer.WriteValue(value.ID);

      writer.WritePropertyName("type");
      writer.WriteValue(value.Type);

      writer.WritePropertyName("name");
      writer.WriteValue(value.Name);

      writer.WritePropertyName("side");
      writer.WriteValue(value.Side);

      writer.WritePropertyName("isPlayer");
      writer.WriteValue(value.IsPlayer);

      writer.WritePropertyName("timeline");
      serializer.Serialize(writer, value.Timeline);

      writer.WriteEndObject();
    }
  }
}
