﻿using Newtonsoft.Json;
using System;

namespace TCS_BattleTracker.Converters {
  class TimelineConverter : JsonConverter<Timeline> {
    public override bool CanRead => false;

    public override bool CanWrite => base.CanWrite;

    public override Timeline ReadJson(JsonReader reader, Type objectType, Timeline existingValue, bool hasExistingValue, JsonSerializer serializer) {
      throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, Timeline value, JsonSerializer serializer) {
      writer.WriteStartObject();

      writer.WritePropertyName("positions");
      writer.WriteStartArray();
      foreach(var position in value.Positions) {
        writer.WriteStartObject();
        writer.WritePropertyName("timestamp");
        writer.WriteValue(position.Timestamp);

        writer.WritePropertyName("transform");
        serializer.Serialize(writer, position.Transform);
        //writer.WriteValue(position.Transform);

        writer.WriteEndObject();
      }
      writer.WriteEndArray();

      writer.WritePropertyName("events");
      writer.WriteStartArray();
      foreach (var evt in value.Events) {
        serializer.Serialize(writer, evt);
      }
      writer.WriteEndArray();

      writer.WriteEndObject();
    }
  }
}
