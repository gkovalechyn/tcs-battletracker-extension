﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Converters {
  class EventConverter : JsonConverter<Event> {
    public override bool CanRead => false;

    public override bool CanWrite => true;

    public override Event ReadJson(JsonReader reader, Type objectType, Event existingValue, bool hasExistingValue, JsonSerializer serializer) {
      //TODO implement
      throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, Event value, JsonSerializer serializer) {
      writer.WriteStartObject();

      writer.WritePropertyName("type");
      writer.WriteValue(value.getType());

      writer.WritePropertyName("timestamp");
      writer.WriteValue(value.Timestamp);

      switch (value) {
        case CreatedEvent e:
          break;
        case DeletedEvent e:
          break;
        case DiedEvent e:
          break;
        case FiredEvent e:
          writer.WritePropertyName("firedPosition");
          serializer.Serialize(writer, e.FiredPosition);

          writer.WritePropertyName("projectileHitPosition");
          serializer.Serialize(writer, e.ProjectileHitPosition);

          writer.WritePropertyName("projectileHitTimestamp");
          writer.WriteValue(e.ProjectileHitTimestamp);
          break;
        case HitEvent e:
          writer.WritePropertyName("instigatorID");
          writer.WriteValue(e.InstigatorID);

          writer.WritePropertyName("instigatorPosition");
          serializer.Serialize(writer, e.InstigatorPos);

          writer.WritePropertyName("myPosition");
          serializer.Serialize(writer, e.MyPosition);
          break;
        case KillEvent e:
          writer.WritePropertyName("victimID");
          writer.WriteValue(e.VictimID);

          writer.WritePropertyName("victimPosition");
          serializer.Serialize(writer, e.VictimPos);

          writer.WritePropertyName("myPosition");
          serializer.Serialize(writer, e.KillerPos);
          break;
      }

      writer.WriteEndObject();
    }


  }
}
