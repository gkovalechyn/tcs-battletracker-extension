﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Converters {
  class TransformConverter : JsonConverter<Transform> {
    public override bool CanRead => false;

    public override bool CanWrite => base.CanWrite;

    public override Transform ReadJson(JsonReader reader, Type objectType, Transform existingValue, bool hasExistingValue, JsonSerializer serializer) {
      throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, Transform value, JsonSerializer serializer) {
      writer.WriteStartObject();

      writer.WritePropertyName("position");
      writer.WriteStartObject();
      writer.WritePropertyName("x");
      writer.WriteValue(value.Position.x);
      writer.WritePropertyName("y");
      writer.WriteValue(value.Position.y);
      writer.WritePropertyName("z");
      writer.WriteValue(value.Position.z);
      writer.WriteEndObject();

      writer.WritePropertyName("rotation");
      writer.WriteValue(value.Rotation);

      writer.WriteEndObject();
    }
  }
}
