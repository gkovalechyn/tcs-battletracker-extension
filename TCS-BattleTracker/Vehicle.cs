﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker {
  public class Vehicle {
    public long ID { get; private set; }
    public VehicleType Type { get; private set; }
    public string Name { get; private set; }
    public bool IsPlayer { get; set; }
    public string Side { get; set; }
    public Timeline Timeline { get; private set; }
    

    public Vehicle(long iD, VehicleType type) {
      this.ID = iD;
      this.Type = type;
      this.Timeline = new Timeline();
    }

    public Vehicle(long iD, VehicleType type, string name) : this(iD, type) {
      this.Name = name;
    }

    public void recordPosition(float timestamp, Vector3 position, float direction) {
      this.Timeline.recordTransform(timestamp, new Transform(position, direction));
    }

    public void recordEvent(Event evt) {
      this.Timeline.recordEvent(evt);
    }
  }
}
