﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using TCS_BattleTracker.Converters;

namespace TCS_BattleTracker {
  
  public class RecordingExporter {
    public bool IsExporting {get; private set;}

    public void beginExport(MissionData missionData, Dictionary<long, Vehicle> units) {
      string dateString = DateTime.Now.ToString("[yyyy-MM-dd]");
      string filename = dateString + missionData.WorldName + "-" + missionData.MissionName + ".json";

      ThreadData data = new ThreadData();
      data.missionData = missionData;
      data.units = units;
      data.filename = filename;

      Thread thread = new Thread(RecordingExporter.threadWorkFunction);

      Logger.debug("Starting export of mission data.");
      Logger.debug("Total unit count: " + units.Count);
      thread.Start(data);
    }

    private static void threadWorkFunction(object obj) {
      try {
        ThreadData data = (ThreadData)obj;
        JsonSerializer serializer = new JsonSerializer();
        serializer.Converters.Add(new VehicleConverter());
        serializer.Converters.Add(new TimelineConverter());
        serializer.Converters.Add(new EventConverter());
        serializer.Converters.Add(new TransformConverter());

        JObject missionDataObject = new JObject();
        //TODO do this better
        JObject worldSizeObject = new JObject();
        worldSizeObject["x"] = data.missionData.WorldSize.x;
        worldSizeObject["y"] = data.missionData.WorldSize.y;
        worldSizeObject["z"] = data.missionData.WorldSize.z;

        missionDataObject["world"] = data.missionData.WorldName;
        missionDataObject["worldSize"] = worldSizeObject;
        missionDataObject["mission"] = data.missionData.MissionName;
        missionDataObject["timestamp"] = data.missionData.Timestamp;

        JArray unitsArray = new JArray();
        
        using (StreamWriter sw = new StreamWriter(data.filename))
        using (JsonWriter writer = new JsonTextWriter(sw)) {
          serializer.Serialize(writer, new {
            missionData = missionDataObject,
            units = data.units
          });
        }
        Logger.debug("Done exporting mission data");
      } catch(Exception e) {
        Logger.error(e.ToString());
      }
    }

    private struct ThreadData {
      public MissionData missionData;

      public Dictionary<long, Vehicle> units;

      public string filename;
    };
  }
}
