﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Commands {
  class VehicleHit : Command {
    /// <summary>
    /// Handles the command where a unit was hit by something.
    /// </summary>
    /// <param name="args">
    ///   0 - (long) Instigator ID
    ///   1 - (vec3) Instigator position
    ///   2 - (long) Hit unit ID
    ///   3 - (vec3) Hit unit Position
    ///   4 - (float) timestamp
    /// </param>
    /// <exception cref="NotImplementedException"></exception>
    void Command.handle(string[] args) {
      Vehicle instigator = BattleTracker.Instance.getVehicle(long.Parse(args[0]));
      if (instigator == null) {
        Logger.warning("VehicleHit::handle - Instigator vehicle " + args[0] + " was either dead or was never tracked");
        return;
      }

      Vehicle hit = BattleTracker.Instance.getVehicle(long.Parse(args[2]));
      if (hit == null) {
        Logger.warning("VehicleHit::handle - Hit vehicle " + args[2] + " was either dead or was never tracked");
        return;
      }

      HitEvent hitEvent = new HitEvent(float.Parse(args[4]), instigator.ID, Vector3.fromArma3Array(args[1]), Vector3.fromArma3Array(args[3]));
      hit.recordEvent(hitEvent);
    }
  }
}
