﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Commands {
  class EntityDeleted : Command {
    /// <summary>
    /// Command called when a vehicle is deleted
    /// </summary>
    /// 
    /// <param name="args">
    ///   0 - (long) The vehicle ID that was just destroyed
    ///   1 - (float) Timestamp
    /// </param>
    /// <exception cref="System.NotImplementedException"></exception>
    void Command.handle(string[] args) {
      long id = long.Parse(args[0]);
      Vehicle vehicle = BattleTracker.Instance.getVehicle(id);

      if (vehicle == null) {
        vehicle = BattleTracker.Instance.getDeadVehicle(id);
      }

      if (vehicle == null) {
        Logger.warning("EntityDeleted::handle - Tried to delete a vehicle that was never tracked");
        return;
      }

      vehicle.recordEvent(new DeletedEvent(float.Parse(args[1])));
    }
  }
}
