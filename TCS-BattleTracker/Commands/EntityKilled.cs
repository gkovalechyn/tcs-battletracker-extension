﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Commands {
  class EntityKilled : Command {
    /// <summary>
    /// Handles the unitKilled command
    /// </summary>
    /// <param name="args">
    ///   0 - (long) Killer ID
    ///   1 - (vec3) Killer position
    ///   2 - (long) Victim ID
    ///   3 - (vec3) Victim Pos
    ///   4 - (float) timestamp
    /// </param>
    /// <exception cref="NotImplementedException"></exception>
    void Command.handle(string[] args) {
      Vehicle killer = BattleTracker.Instance.getVehicle(long.Parse(args[0]));

      if (killer == null) {
        Logger.warning("EntityKilled::handle - Killer " + args[0] + " is not alive or was never registered");
      }

      Vehicle victim = BattleTracker.Instance.getVehicle(long.Parse(args[2]));

      if (victim == null) {
        Logger.warning("EntityKilled::handle - Victim " + args[2] + " is not alive or was never registered");
        return;
      }

      if (killer != null) {
        killer.recordEvent(new KillEvent(float.Parse(args[4]), Vector3.fromArma3Array(args[1]), victim.ID, Vector3.fromArma3Array(args[3])));
      }

      victim.recordEvent(new DiedEvent(float.Parse(args[4])));

      BattleTracker.Instance.vehicleDied(victim.ID);
    }
  }
}
