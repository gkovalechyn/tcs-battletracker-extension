﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Commands {
  interface Command {
    void handle(string[] args);
  }
}
