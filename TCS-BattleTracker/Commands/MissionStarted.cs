﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Commands {
  class MissionStarted : Command {
    /// <summary>
    /// Handles the Mission start command
    /// </summary>
    /// <param name="args">
    ///   0 - (string) The world name
    ///   1 - (vec2) World size
    ///   2 - (string) The mission name
    ///   3 - (number) Timestamp
    /// </param>
    void Command.handle(string[] args) {
      if (args.Length != 4) {
        Logger.error("MissionStarted::handle - Expected 4 arguments but received " + args.Length);
        return;
      }

      //Mission and world name have the " character around them, so we need to remove those
      var realWorldName = args[0].Substring(1, args[0].Length - 2).Trim();
      var worldSize = Vector3.fromArma3Array(args[1]);
      var realMisisonName = args[2].Substring(1, args[2].Length - 2).Trim();
      float timestamp = float.Parse(args[3]);

      MissionData data = new MissionData();
      data.WorldName = realWorldName;
      data.WorldSize = worldSize;
      data.MissionName = realWorldName;
      data.Timestamp = timestamp;

      BattleTracker.Instance.missionStarted(data);
    }
  }
}
