﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Commands {
  class MissionEnded : Command {
    /// <summary>
    /// Handles the mission end command
    /// </summary>
    /// <param name="args">
    ///   0 - (float) Timestamp of the mission end
    /// </param>
    void Command.handle(string[] args) {
      BattleTracker.Instance.missionEnded(float.Parse(args[0]));
    }
  }
}
