﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Commands {
  class UpdateVehicle : Command {
    /// <summary>
    /// Called when a vehicle's position needs to be updated
    /// </summary>
    /// 
    /// <param name="args">
    ///   0 - (long) The vehicle ID
    ///   1 - (Vec3) The new position
    ///   2 - (float) The new rotation
    ///   3 - (float) Timestamp
    /// </param>
    /// <exception cref="System.NotImplementedException"></exception>
    void Command.handle(string[] args) {
      Vehicle vehicle = BattleTracker.Instance.getVehicle(long.Parse(args[0]));

      if (vehicle == null) {
        Logger.warning("UpdateVehicle::handle - Tried to update vehicle " + args[0] + " that is not alive or was never registered");
        return;
      }

      vehicle.recordPosition(float.Parse(args[3]), Vector3.fromArma3Array(args[1]), float.Parse(args[2]));
    }
  }
}
