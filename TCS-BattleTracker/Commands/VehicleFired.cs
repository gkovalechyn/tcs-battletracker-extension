﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Commands {
  class VehicleFired : Command {
    /// <summary>
    /// Command handler for a when a vehicle fires a weapon and the bullet impacts somewhere.
    /// </summary>
    /// <param name="args">
    ///   0 - (long) Unit ID who just fired
    ///   1 - (vec3) Where the unit fired from
    ///   2 - (float) When the unit fired
    ///   3 - (vec3) Where the projectile hit
    ///   4 - (float) When the projectile hit
    /// </param>
    /// 
    void Command.handle(string[] args) {
      Vehicle vehicle = BattleTracker.Instance.getVehicle(long.Parse(args[0]));

      if (vehicle == null) {
        Logger.warning("VehicleFired::handle - Received fired event for unit " + args[0] + " that was either dead or was never tracked");
        return;
      }

      FiredEvent evt = new FiredEvent(float.Parse(args[2]), Vector3.fromArma3Array(args[1]), Vector3.fromArma3Array(args[3]), float.Parse(args[4]));
      vehicle.recordEvent(evt);
    }
  }
}
