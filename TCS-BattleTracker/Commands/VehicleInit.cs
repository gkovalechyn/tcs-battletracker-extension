﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker.Commands {
  class VehicleInit : Command {
    /// <summary>
    /// Called when a vehicle was spawned. 
    /// 
    /// @TODO This does not handle units spawning inside a vehicle
    /// </summary>
    /// 
    /// <param name="args">
    ///   0 - (long) The unit ID
    ///   1 - (int) The vehicle type <see cref="VehicleType"/>
    ///   2 - (string) The vehicle name
    ///   3 - (Vec3) The initial position
    ///   4 - (float) The initial rotation
    ///   5 - (string) The object's side
    ///   6 - (boolean) Is the vehicle a player
    ///   7 - (float) Timestamp
    /// </param>
    /// <exception cref="System.NotImplementedException"></exception>
    void Command.handle(string[] args) {
      if (args.Length != 8) {
        Logger.warning("VehicleInit - Wrong number of arguments, expected 7 but received " + args.Length);
        return;
      }

      long vehicleID = long.Parse(args[0]);
      VehicleType type = (VehicleType)int.Parse(args[1]);

      //Vehicle names may or may not start and end with " depending if they have a name or not
      string vehicleName;

      if (args[2].StartsWith("\"")) {
        vehicleName = args[2].Substring(1, args[2].Length - 2).Trim();
      } else {
        vehicleName = args[2];
      }
      

      Vector3 spawnPos = Vector3.fromArma3Array(args[3]);
      float direction = float.Parse(args[4]);
      string side = args[5];
      bool isPlayer = args[6] == "true";
      float timestamp = float.Parse(args[7]);

      Vehicle vehicle = new Vehicle(vehicleID, type, vehicleName);
      vehicle.Side = side;
      vehicle.IsPlayer = isPlayer;

      vehicle.recordPosition(timestamp, spawnPos, direction);
      vehicle.recordEvent(new CreatedEvent(timestamp));

      BattleTracker.Instance.vehicleCreated(vehicle);
    }
  }
}
