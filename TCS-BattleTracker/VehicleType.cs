﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker {
  public enum VehicleType {
    MAN = 0,
    HELICOPTER = 1,
    PLANE = 2,
    CAR = 3,
    TANK = 4,
    SHIP = 5,
    STATIC_WEAPON = 6
  }
}
