﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TCS_BattleTracker.Commands;

namespace TCS_BattleTracker {
  public class BattleTracker {
    private static BattleTracker _instance;
    private Dictionary<string, Command> commands = new Dictionary<string, Command>();
    private RecordingExporter exporter = new RecordingExporter();

    private Dictionary<long, Vehicle> aliveUnits = new Dictionary<long, Vehicle>();
    private Dictionary<long, Vehicle> deadUnits = new Dictionary<long, Vehicle>();
    private MissionData missionData;
    private bool isMissionRunning = false;

    public BattleTracker() {
      this.registerCommands();

      Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
      Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
    }

    public static BattleTracker Instance {
      get {
        if (BattleTracker._instance == null) {
          BattleTracker._instance = new BattleTracker();
        }

        return BattleTracker._instance;
      }
    }

    public void handleCommand(StringBuilder builder, string function, string[] args) {
      Command command;

      //Logger.debug("Function called: " + function);
      //Logger.debug("Arguments:");

      //foreach(string arg in args) {
      //  Logger.debug(arg);
      //}

      if (this.commands.TryGetValue(function, out command)) {
        try {
          command.handle(args);
        }catch(Exception e) {
          Logger.error(e.ToString());
          Logger.error("Function: " + function);
          Logger.error("Arguments:");
          foreach(var arg in args) {
            Logger.error(arg);
          }
        }
      } else {
        Logger.error("Tried to execute command \"" + function + "\" that does not exist");
      }
    }

    private void registerCommands() {
      this.commands["updateVehicle"] = new UpdateVehicle();

      this.commands["missionStarted"] = new MissionStarted();
      this.commands["missionEnded"] = new MissionEnded();

      this.commands["vehicleInit"] = new VehicleInit();
      this.commands["vehicleFired"] = new VehicleFired();
      this.commands["vehicleHit"] = new VehicleHit();

      this.commands["entityKilled"] = new EntityKilled();
      this.commands["entityDeleted"] = new EntityDeleted();
    }

    public void vehicleCreated(Vehicle vehicle) {
      if (this.aliveUnits.ContainsKey(vehicle.ID) || this.deadUnits.ContainsKey(vehicle.ID)) {
        Logger.error("Tried to create vehicle with ID " + vehicle.ID + " twice");
      }

      this.aliveUnits[vehicle.ID] = vehicle;
    }

    public void vehicleDied(long id) {
      Vehicle vehicle;

      if (this.aliveUnits.TryGetValue(id, out vehicle)) {
        if (this.deadUnits.ContainsKey(id)) {
          Logger.error("Tried to set vehicle with id " + id + " as dead but it was already marked as dead");
        } else {
          this.aliveUnits.Remove(id);
          this.deadUnits[id] = vehicle;
        }
      } else {
        Logger.error("Tried to set vehicle with id " + id + " as dead but it was never tracked as alive");
      }
    }

    public Vehicle getVehicle(long id) {
      Vehicle vehicle;

      this.aliveUnits.TryGetValue(id, out vehicle);

      return vehicle;
    }

    public Vehicle getDeadVehicle(long id) {
      Vehicle vehicle;

      this.deadUnits.TryGetValue(id, out vehicle);

      return vehicle;
    }

    public void missionStarted(MissionData data) {
      if (this.isMissionRunning) {
        Logger.warning("BattleTracker::missionStarted - Function called but there is a mission already running");
        Logger.warning("BattleTracker::missionStarted - Cleaning up and starting again");
        this.aliveUnits.Clear();
        this.deadUnits.Clear();
      }

      this.missionData = data;

      this.isMissionRunning = true;
    }

    public void missionEnded(float timestamp) {
      if (!this.isMissionRunning) {
        Logger.warning("BattleTracker::missionEnded - Function called but there no mission running");
        return;
      }
      Dictionary<long, Vehicle> allVehicles = new Dictionary<long, Vehicle>(this.aliveUnits);
      
      foreach(var entry in this.deadUnits){
        allVehicles[entry.Key] = entry.Value;
      }

      this.aliveUnits.Clear();
      this.deadUnits.Clear();

      this.exporter.beginExport(this.missionData, allVehicles);

      this.isMissionRunning = false;
    }

  }
}
