﻿namespace TCS_BattleTracker {
  public class Transform {
    public Vector3 Position { get; set; }
    public float Rotation { get; set; }

    public Transform(Vector3 position, float rotation) {
      this.Position = position;
      this.Rotation = rotation;
    }
  }
}
