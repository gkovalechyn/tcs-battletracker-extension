﻿namespace TCS_BattleTracker {
  public class TransformAtTimepoint {
    public float Timestamp { get; set; }
    public Transform Transform { get; set; }

    public TransformAtTimepoint(float timestamp, Transform transform) {
      this.Timestamp = timestamp;
      this.Transform = transform;
    }
  }
}