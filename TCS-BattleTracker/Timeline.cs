﻿using System;
using System.Collections.Generic;
using TCS_BattleTracker.Events;

namespace TCS_BattleTracker {
  public class Timeline {
    public List<TransformAtTimepoint> Positions { get; private set; }
    public List<Event> Events { get; private set; }

    public Timeline() {
      this.Positions = new List<TransformAtTimepoint>();
      this.Events = new List<Event>();
    }

    public void recordTransform(float timestamp, Transform transform) {
      //If we have a point and can reuse it, do it. Otherwise add a new point
      if (this.Positions.Count > 1) {
        TransformAtTimepoint lastPoint = this.Positions[this.Positions.Count - 1];

        //Optimize some records so many aren't generated for static or standing still units
        if (
            Math.Abs(lastPoint.Transform.Rotation - transform.Rotation) < 5
            && (lastPoint.Transform.Position - transform.Position).squaredMagnitude() < 0.5
          ) {//Arbitrary value, more testing needed

          lastPoint.Transform = transform;
          lastPoint.Timestamp = timestamp;
          return;
        }
      }

      this.Positions.Add(new TransformAtTimepoint(timestamp, transform));
    }

    public void recordEvent(Event evt) {
      this.Events.Add(evt);
    }
  }
}