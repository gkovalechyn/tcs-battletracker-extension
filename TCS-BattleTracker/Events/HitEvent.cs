﻿namespace TCS_BattleTracker.Events {
  class HitEvent : Event {
    public long InstigatorID { get; private set; }
    public Vector3 InstigatorPos { get; private set; }

    public Vector3 MyPosition { get; private set; }

    public HitEvent(float timestamp, long instigatorID, Vector3 instigatorPos, Vector3 myPos) : base(timestamp){
      this.InstigatorID = instigatorID;
      this.InstigatorPos = instigatorPos;
      this.MyPosition = myPos;
    }

    public override string getType() {
      return "Hit";
    }
  }
}
