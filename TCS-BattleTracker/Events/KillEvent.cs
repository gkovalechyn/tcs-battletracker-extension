﻿namespace TCS_BattleTracker.Events {
  public class KillEvent : Event {
    public Vector3 KillerPos { get; private set; }

    public long VictimID { get; private set; }
    public Vector3 VictimPos { get; private set; }

    public KillEvent(float timestamp, Vector3 killerPos, long victimID, Vector3 victimPos) : base(timestamp) {
      this.KillerPos = killerPos;
      this.VictimID = victimID;
      this.VictimPos = victimPos;
    }

    public override string getType() {
      return "Kill";
    }
  }
}
