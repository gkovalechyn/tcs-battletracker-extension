﻿namespace TCS_BattleTracker.Events {
  class FiredEvent : Event {
    public Vector3 FiredPosition { get; private set; }
    public Vector3 ProjectileHitPosition { get; private set; }
    public float ProjectileHitTimestamp { get; private set; }

    public FiredEvent(float timestamp, Vector3 firedPosition, Vector3 projectileHitPosition, float projectileHitTimestamp): base(timestamp) {
      this.FiredPosition = firedPosition;
      this.ProjectileHitPosition = projectileHitPosition;
      this.ProjectileHitTimestamp = projectileHitTimestamp;
    }

    public override string getType() {
      return "Fired";
    }
  }
}
