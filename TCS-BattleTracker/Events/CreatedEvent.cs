﻿namespace TCS_BattleTracker.Events {
  public class CreatedEvent : Event {
    public CreatedEvent(float timestamp) : base(timestamp) {

    }

    public override string getType() {
      return "Created";
    }
  }
}
