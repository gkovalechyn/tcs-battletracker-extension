﻿namespace TCS_BattleTracker.Events {
  class DiedEvent : Event {
    public DiedEvent(float timestamp) : base(timestamp) {
    }

    public override string getType() {
      return "Died";
    }
  }
}
