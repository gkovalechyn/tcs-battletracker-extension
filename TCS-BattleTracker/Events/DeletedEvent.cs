﻿namespace TCS_BattleTracker.Events {
  class DeletedEvent : Event {
    public DeletedEvent(float timestamp) : base(timestamp) {
    }

    public override string getType() {
      return "Deleted";
    }
  }
}
