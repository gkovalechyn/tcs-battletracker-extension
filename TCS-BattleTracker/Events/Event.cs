﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCS_BattleTracker.Events {
  public abstract class Event {
    public float Timestamp { get; private set; }

    public Event(float timestamp) {
      this.Timestamp = timestamp;
    }

    public abstract string getType();
  }
}
